<form action="POST" v-on:submit.prevent="createSensor();">
    <div class="modal fade" id="create">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                <h4>Crear</h4>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <label for="sensor">Crear Sensor</label><br />
                    <label>Description</label>
                    <input type="text" name="description" class="form-control" v-model="description">
                    <label>Eje X</label>
                    <input type="text" name="ejeX" class="form-control" v-model="ejeX">
                    <label>Eje Y</label>
                    <input type="text" name="ejeY" class="form-control" v-model="ejeY">
                    <span v-for="error in errors" class="text-danger">@{{error}}</span>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" value="Guardar">
                </div>
            </div>
        </div>
    </div>
</form>