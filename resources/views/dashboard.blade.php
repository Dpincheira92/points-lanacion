@extends('app')

@section('content')

<div id="crud" class="row">
    <div class="col-xs-12">
        <h1 class="page-header">CRUD Sensores</h1>
    </div>
    <div class="col-sm-7">
        <a href="#" class="btn btn-primary pull-right" data-toggle="modal" data-target="#create">Nuevo Sensor</a>
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Descripcion</th>
                        <th>Coordenadas</th>
                        <th colspan="2">
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for ="sensor in sensors">
                        <td width="10px">@{{sensor.id}}</td>
                        <td>@{{sensor.description}}</td>
                        <td>@{{sensor.ejeX}}, @{{sensor.ejeY}}</td>
                        <td width="10px">
                            <a href="#" class="btn btn-warning btn-sm" v-on:click.prevent="editSensor(sensor);">Editar</a>
                        </td>
                        <td width="10px">
                            <a href="#" class="btn btn-danger btn-sm" v-on:click.prevent="deleteSensor(sensor);">Eliminar</a>
                        </td>
                        
                      
                        <?php echo App\Http\Controllers\SensorController::prueba();?>


                    </tr>
                </tbody>
            </table>
        @include('create')
        @include('edit')

        
    </div>
</div>

@endsection