<form action="PUT" v-on:submit.prevent="updateSensor(fillSensor.id);">
    <div class="modal fade" id="edit">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                <h4>Editar</h4>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <label for="point">Actualizar Tarea</label><br />
                    <label>Description</label>
                    <input type="text" name="point" class="form-control" v-model="fillSensor.description">
                    <label>Eje X</label>
                    <input type="text" name="point" class="form-control" v-model="fillSensor.ejeX">
                    <label>Eje Y</label>
                    <input type="text" name="point" class="form-control" v-model="fillSensor.ejeY">
                    <span v-for="error in errors" class="text-danger">@{{ error }}</span>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" value="Actualizar">
                </div>
            </div>
        </div>
    </div>
</form>