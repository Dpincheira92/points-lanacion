new Vue({
    el: '#crud',
    created: function(){
        this.getSensors();
    },
    data: {
        sensors:[],
        description: '',
        ejeX: '',
        ejeY: '',
        errors: [],
        fillSensor: {'id': '', 'description': '', 'ejeX': '', 'ejeY': ''}
    },
    methods:{
        getSensors: function(){
            var urlSensors = 'sensors';
            axios.get(urlSensors).then(response => {
                this.sensors = response.data
            })
        },
        deleteSensor: function(sensor){
            var url = 'sensors/' + sensor.id;
            axios.delete(url).then(response =>{
                this.getSensors();
                toastr.success('eliminado!');
            })
        },
        createSensor: function(){
            var url = 'sensors';
            axios.post(url, {
                description: this.description,
                ejeX: this.ejeX,
                ejeY: this.ejeY
            }).then(response => {
                this.getSensors();
                this.description = '';
                this.ejeX = '';
                this.ejeY = '';
                this.errors = '';
                $('#create').modal('hide');
                toastr.success('creado con exito!');
            }).catch(error => {
                this.error = error.response.data
            })
        },
        editSensor: function(sensor){
            this.fillSensor.id = sensor.id;
            this.fillSensor.description = sensor.description;
            this.fillSensor.ejeX = sensor.ejeX;
            this.fillSensor.ejeY = sensor.ejeY;
            $('#edit').modal('show');
        },
        updateSensor: function(id){
            var url = 'sensors/'+ id;
            axios.put(url, this.fillSensor).then(response=>{
                this.getSensors();
                this.fillSensor = {'id': '', 'description': '', 'ejeX': '', 'ejeY': ''};
                this.errors = [];
                $('#edit').modal('hide');
                toastr.success('datos actualizados!');
            }).catch(error => {
                this.errors = error.response.data;
            })
        }
    }
});